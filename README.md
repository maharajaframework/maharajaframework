# README #

The MahaRAJA framework is a software tool for Event-Based Runtime Verification of Real-time Java Programs.

### Contributors ###

* Matteo Camilli (matteo.camilli@unimi.it)
* Carlo Bellettini (carlo.bellettini@unimi.it)
* Angelo Gargantini (angelo.gargantini@unibg.it)
* Patrizia Scandurra (patrizia.scandurra@unibg.it)

### How do I get set up? ###

The MahaRAJA framework is a Java software tool.
It requires:

* The [AspectJ](https://eclipse.org/aspectj/) runtime library
* The Apache [commons CLI](https://commons.apache.org/proper/commons-cli/) library
* The [Java thread affinity](https://github.com/OpenHFT/Java-Thread-Affinity) library
* The [Graphgen](http://camilli.di.unimi.it/maharaja/graphgen-lib.jar) library

### License ###

MahaRAJA is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

MahaRAJA is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.