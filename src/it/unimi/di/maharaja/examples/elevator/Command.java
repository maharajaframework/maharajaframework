/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.elevator;

import it.unimi.di.maharaja.monitor.AfterCheck;


enum TYPE {
	MOVE,
	OPEN,
	OPEN_ACT_EVENT,
	CLOSE_ACT_EVENT,
	APPROACHING,
	TIME_OUT,
	INTERRUPT,
	EXIT
};

public class Command {

	private int floor;
	private TYPE type;
	
	private Command(TYPE t, int f){
		type = t;
		floor = f;
	}
	
	private Command(TYPE t){
		type = t;
	}
	
	public int getFloor(){
		return floor;
	}
	
	public static Command getMoveCommand(int f){
		return new Command(TYPE.MOVE, f);
	}
	
	public static Command getOpenCommand(){
		return new Command(TYPE.OPEN);
	}
	
	public static Command getOpenActEventCommand(){
		return new Command(TYPE.OPEN_ACT_EVENT);
	}
	
	public static Command getCloseActEventCommand(){
		return new Command(TYPE.CLOSE_ACT_EVENT);
	}
	
	public static Command getTimeOutCommand(){
		return new Command(TYPE.TIME_OUT);
	}
	
	public static Command getInterruptCommand(){
		return new Command(TYPE.INTERRUPT);
	}
	
	public static Command getExitCommand(){
		return new Command(TYPE.EXIT);
	}
	
	public static Command getApproachingCommand() {
		return new Command(TYPE.APPROACHING);
	}
	
	
	public boolean isMove(){
		return type == TYPE.MOVE;
	}
	
	public boolean isOpen(){
		return type == TYPE.OPEN;
	}
	
	public boolean isInterrupt(){
		return type == TYPE.INTERRUPT;
	}
	
	public boolean isExit(){
		return type == TYPE.EXIT;
	}

	public boolean isValidMovement(int currentFloor) {
		return floor != currentFloor;
	}
	
	@AfterCheck(name="process")
	public int floors(int currentFloor){
		return Math.abs(floor - currentFloor);
	}

	public boolean isOpenActEvent() {
		return type == TYPE.OPEN_ACT_EVENT;
	}
	
	public boolean isCloseActEvent() {
		return type == TYPE.CLOSE_ACT_EVENT;
	}
	
	public boolean isTimeOut() {
		return type == TYPE.TIME_OUT;
	}
	
	public boolean isApproaching() {
		return type == TYPE.APPROACHING;
	}
	
}
