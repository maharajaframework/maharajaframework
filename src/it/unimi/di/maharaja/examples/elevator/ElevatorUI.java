/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.elevator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import net.openhft.affinity.AffinityLock;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import it.unimi.di.maharaja.monitor.ControllableActionCheckPoint;
import it.unimi.di.maharaja.monitor.Monitor;
import it.unimi.di.maharaja.monitor.Monitorable;

public class ElevatorUI extends Monitorable {
	
	private static final Logger log = Logger.getLogger(ElevatorUI.class.getName());
	
	private Engine engine = null;
	
	public ElevatorUI(){
		engine = Engine.getInstance();
	}
	
	private void interactiveMode(){
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in)); 
        String line;
        try {
			while ((line = stdin.readLine()) != null && !line.equalsIgnoreCase("exit")) {
			    if(isNumeric(line)){
			    	int f = Integer.parseInt(line);
			    	if(engine.isValidFloor(f))
			    		engine.putCommand(Command.getMoveCommand(f));
			    }
			    else if(line.equalsIgnoreCase("int"))
			    	engine.putCommand(Command.getInterruptCommand());
			    else if(line.equalsIgnoreCase("open"))
			    	engine.putCommand(Command.getOpenCommand());
			}
			engine.putCommand(Command.getExitCommand());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static boolean isNumeric(String str) {  
		try  
		{  
			Integer.parseInt(str);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true;  
	}
	
	private void automaticMode(){
		try {
			for(int i=0; i<5; i++){
				engine.putCommand(Command.getOpenCommand());
				Thread.sleep(5000);
				engine.putCommand(Command.getMoveCommand(3));
				Thread.sleep(1000);
				engine.putCommand(Command.getMoveCommand(5));
				Thread.sleep(23000);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(1000);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(300);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(2000);
				engine.putCommand(Command.getMoveCommand(1));
				Thread.sleep(2000);
				engine.putCommand(Command.getOpenCommand());
				Thread.sleep(7000);
				engine.putCommand(Command.getOpenCommand());
				Thread.sleep(5000);
				engine.putCommand(Command.getMoveCommand(3));
				Thread.sleep(7000);
				engine.putCommand(Command.getMoveCommand(5));
				Thread.sleep(1000);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(1000);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(300);
				engine.putCommand(Command.getInterruptCommand());
				Thread.sleep(2000);
				engine.putCommand(Command.getMoveCommand(1));
				Thread.sleep(55000);
			}
			
			engine.putCommand(Command.getExitCommand());
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run(Object... args) {
		engine.setAffinityLock((AffinityLock)args[0]);
		engine.start();
		
		//interactiveMode();
		automaticMode();
	}
	
	public static void main(String args[]){
		Long start = System.nanoTime();
		
		String inputPath = "";
    	
    	// create the command line parser
		CommandLineParser parser = new BasicParser();
		HelpFormatter formatter = new HelpFormatter();

		// create the Options
		Options options = new Options();
		options.addOption( "h", "help", false, "Show the basic usage.");
		//options.addOption( "t", "test", false, "Set online testing mode.");
		options.addOption( "m", "model", true, "Set the TB net model path.");


		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );

		    // validate that bucket-name has been set
		    if(line.hasOption("help") || !line.hasOption("model")) {
		    	formatter.printHelp( "ElevatorUI [OPTION] ...", options );
				System.exit(0);
		    }
		    
		    if(line.hasOption("model"))
		    	inputPath = line.getOptionValue("model");
//		    if(line.hasOption("test"))
//		    	Monitor.setOnlineTestingMode(true);
		    
		}
		catch( Exception exp ) {
		    exp.printStackTrace();
		}
		
		Monitor.startMonitorFromFile(new ElevatorUI(), inputPath ,(Object[])args);
		//new ElevatorUI().run((Object[])args);
		
		log.info("Exec. nano time = " + (System.nanoTime()-start) + " ns.");
	}

	
	@Override
	public void onlineTest(Object... args) {
		engine.setAffinityLock((AffinityLock)args[0]);
		engine.start();	
		
		while(true){
			List<String> actions = ControllableActionCheckPoint.getInstance().waitForControllableActions();
			String selectedAction = null;
			if(actions.contains("interrupt") || actions.contains("interrupt2"))
				selectedAction = ControllableActionCheckPoint.getInstance().randomChoiceWithNone(actions);
			else
				selectedAction = ControllableActionCheckPoint.getInstance().randomChoice(actions);
			
			if(selectedAction.equals("openActReq"))
				engine.putCommand(Command.getOpenCommand());
			else if(selectedAction.equals("interrupt") || selectedAction.equals("interrupt2")) {
				try {
					Thread.sleep((long) (Math.random() * 3000));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				engine.putCommand(Command.getInterruptCommand());
			}
			else if(selectedAction.equals("moveActReq")){
				int randomFloor = new Random().nextInt((5 - 1) + 1) + 1;
				engine.putCommand(Command.getMoveCommand(randomFloor));
			}
				
		}
	}

}
