/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.elevator;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import it.unimi.di.maharaja.monitor.AfterCheck;
import it.unimi.di.maharaja.monitor.AroundCheck;
import it.unimi.di.maharaja.monitor.BeforeCheck;
import net.openhft.affinity.AffinityLock;
import net.openhft.affinity.AffinityStrategies;
import net.openhft.affinity.AffinitySupport;

public class Engine extends Thread {
	
	private boolean doorOpen = false;
	private boolean idle = true;
	private boolean approaching = false;
	private boolean slidingDoor = false;
	private boolean closing = false;
	private boolean countDown = false;
	private int minFloor = 1;
	private int maxFloor = 5;
	private int currentFloor = 1;
	
	private AffinityLock al;
	private ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
	private static final Logger log = Logger.getLogger(Engine.class.getName());
	
	private LinkedBlockingQueue<Command> commands = null;
	private ArrayDeque<Command> moveQueue = null;
	private static Engine instance = null;
	
	private Engine(){
		commands = new LinkedBlockingQueue<Command>();
		moveQueue = new ArrayDeque<Command>();
	}
	
	public static Engine getInstance(){
		if(instance == null)
			instance = new Engine();
		return instance;
	}
	
	@Override
	public void run() {
		super.run();
		try (AffinityLock al2 = al.acquireLock(AffinityStrategies.DIFFERENT_SOCKET, AffinityStrategies.DIFFERENT_CORE, AffinityStrategies.ANY)) {
			log.info("[" + AffinitySupport.getThreadId() + "] Engine locked: CPU " + al2.cpuId());
			
//			ThreadMXBean bean = ManagementFactory.getThreadMXBean();
//			long start = bean.getThreadCpuTime(Thread.currentThread().getId());
			
			while(true){
				try {
					Command event = commands.take();
					if(event.isOpen() && !doorOpen && (idle || approaching))
						openDoor();
					else if(event.isOpenActEvent() && !closing && slidingDoor)
						openActionEvent();
					else if(event.isTimeOut() && doorOpen && slidingDoor)
						closeDoor();
					else if(event.isInterrupt() && doorOpen && slidingDoor && (closing || countDown))
						interruptDoor();
					else if(event.isApproaching() && !doorOpen && !idle && !approaching)
						approachingFloor();
					else if(event.isCloseActEvent() && closing && doorOpen && slidingDoor) {
						closeActionEvent();
						if(!moveQueue.isEmpty()){
							// pop and handle a move request
							Command mEvent = moveQueue.pollFirst();
							if(mEvent.isValidMovement(currentFloor)){
								moveActRequest();
								int floors = mEvent.floors(currentFloor); // process
								move(floors, mEvent.getFloor());
							}
						}
					}
					else if(event.isMove()){
						if(!doorOpen && idle && event.isValidMovement(currentFloor)){
							// the request can be handled now
							moveActRequest();
							int floors = event.floors(currentFloor); // process
							move(floors, event.getFloor());
						}
						else {
							// enqueue the request
							moveQueue.addLast(event);
						}
					}
					else if(event.isExit())
						break;
					
				}
				catch(InterruptedException e){
					e.printStackTrace();
				}
				
				scheduler.shutdown();
			}
			
//			log.finer("[Consumer] cpu-time " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - start));
		}
	}
	
	@AroundCheck(before="startApproaching1,startApproaching2,startApproaching3,startApproaching4,startApproaching5", after="endApproaching1,endApproaching2,endApproaching3,endApproaching4,endApproaching5")
	private void approachingFloor() {
		log.info("APPROACHING FLOOR");
		approaching = true;
	}

	@BeforeCheck(name="moveActReq")
	private void moveActRequest() {
		log.info("MOVE ACTION REQUEST");
		
	}


	private void move(int floors, int newFloor) {
		log.info("MOVE COMMAND: "+ floors + " FLOORS");
		if(floors == 1)
			process1();
		else if(floors == 2)
			process2();
		else if(floors == 3)
			process3();
		else if(floors == 4)
			process4();
		else if(floors == 5)
			process5();
		
		idle = false;
		//approaching = true;
		currentFloor = newFloor;
		executeAsyncTask(new Movement(this, floors));
	}
	
	@BeforeCheck(name="process5")
	private void process5() {
		log.info("PROCESSING FLOOR 5");
	}
	
	@BeforeCheck(name="process4")
	private void process4() {
		log.info("PROCESSING FLOOR 4");
	}

	@BeforeCheck(name="process3")
	private void process3() {
		log.info("PROCESSING FLOOR 3");
	}

	@BeforeCheck(name="process2")
	private void process2() {
		log.info("PROCESSING FLOOR 2");
		
	}

	@BeforeCheck(name="process1")
	private void process1() {
		log.info("PROCESSING FLOOR 1");
		
	}

	@AfterCheck(name="interrupt,interrupt2")
	private void interruptDoor() {
		log.info("INTERRUPT COMMAND");
		scheduler.shutdownNow();
		scheduler = Executors.newScheduledThreadPool(1);
		
		if(closing){
			closing = false;
			executeAsyncTask(new SlidingDoor(this, true));
		} else
			executeAsyncTask(new TimeOut(this, 3000L));
	}

	@AfterCheck(name="openActReq,openActReq2")
	private void openDoor() {
		log.info("OPEN DOOR COMMAND");
		idle = false;
		approaching = false;
		slidingDoor = true;
		executeAsyncTask(new SlidingDoor(this, true));
	}
	
	@AfterCheck(name="openActEvent")
	public void openActionEvent(){
		log.info("OPEN ACTION EVENT");
		doorOpen = true;
		countDown = true;
		executeAsyncTask(new TimeOut(this, 3000L));
	}
	
	@BeforeCheck(name="timeout")
	private void closeDoor() {
		log.info("CLOSE DOOR COMMAND");
		closing = true;
		countDown = false;
		executeAsyncTask(new SlidingDoor(this, false));
	}
	
	@AfterCheck(name="closeActEvent")
	public void closeActionEvent(){
		log.info("CLOSE ACTION EVENT");
		doorOpen = false;
		slidingDoor = false;
		closing = false;
		idle = true;
	}

	public void setAffinityLock(AffinityLock affinityLock) {
		al = affinityLock;
		
	}
	
	public boolean isValidFloor(int f){
		return f >= minFloor && f <= maxFloor;
	}

	public void putCommand(Command command) {
		try {
			commands.put(command);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void executeAsyncTask(Runnable runnable){
		scheduler = Executors.newScheduledThreadPool(1);
		scheduler.schedule(runnable, 0, TimeUnit.MILLISECONDS);
	}
}
