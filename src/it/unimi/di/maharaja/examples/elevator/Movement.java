/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.elevator;

import java.util.logging.Logger;

import it.unimi.di.maharaja.monitor.AfterCheck;

public class Movement implements Runnable {
	
	private Engine engine;
	private int floors;
	
	private static final Logger log = Logger.getLogger(Movement.class.getName());
	
	public Movement(Engine engine, int floors){
		this.engine = engine;
		this.floors = floors;
	}
	
	@Override
	public void run() {
		startMoving();
		try {
			Thread.sleep(floors * 3000);
		} catch (InterruptedException e) {
			//e.printStackTrace();
		}
		//approaching();
		engine.putCommand(Command.getApproachingCommand());
		engine.putCommand(Command.getOpenCommand());

	}
	
	@AfterCheck(name="startMoving1,startMoving2,startMoving3,startMoving4,startMoving5")
	private void startMoving(){
		log.info("MOVING...");
	}
	

}
