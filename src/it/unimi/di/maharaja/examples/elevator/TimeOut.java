/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.elevator;


public class TimeOut implements Runnable {
	
	private Engine engine;
	private Long time;
	
	public TimeOut(Engine engine, Long timeMillis){
		this.engine = engine;
		this.time = timeMillis;
	}

	@Override
	public void run() {
		boolean interrupted = false;
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			//e.printStackTrace();
			interrupted = true;
		}
		if(!interrupted){
			engine.putCommand(Command.getTimeOutCommand());
		}
	}

}
