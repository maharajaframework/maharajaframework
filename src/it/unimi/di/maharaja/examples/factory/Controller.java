/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.factory;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import it.unimi.di.maharaja.monitor.AfterCheck;
import it.unimi.di.maharaja.monitor.AroundCheck;
import it.unimi.di.maharaja.monitor.BeforeCheck;
import it.unimi.di.maharaja.monitor.Monitor;
import it.unimi.di.maharaja.monitor.Monitorable;


public class Controller extends Monitorable implements IController {
	
	public static final int WORKERS = 4;
	public static final int PRODUCTS = 7;
	public static final String CONTROLLER_NAME = "Controller";
	
	private int producedItems = 0;
	
	private static final Logger log = Logger.getLogger(Controller.class.getName());

	@Override
	public void joinWorker(Integer id) throws RemoteException {
		log.info("Joining...");
		JoinCheckpoint.getInstance().joinWorker(WORKERS, id);
	}
	
	@AfterCheck(name="readyActReq")
	private void workersReady(){
		log.info("All the workers are ready.");
	}

	@Override
	@BeforeCheck(name="receivedActEv")
	public void receivedActionEvent() throws RemoteException {
		log.info("Worker received a task...");
		
	}

	@Override
	@BeforeCheck(name="pickingAct")
	public void pickingAction() throws RemoteException {
		log.info("Worker is picking...");
		
	}

	@Override
	@BeforeCheck(name="assemblyAct")
	public void assemblyAction() throws RemoteException {
		log.info("Worker is assembling...");
		
	}
	
	@Override
	@BeforeCheck(name="doneActEv")
	public void doneAction() throws RemoteException {
		log.info("Worker done...");
		
	}

	@Override
	@AfterCheck(name="placingAct")
	public void placingAction() throws RemoteException {
		log.info("Worker is placing...");
		
	}

	@AroundCheck(before="shippingActReq", after="shippingActEv")
	public void shippingAction() {
		log.info("Shipping produced item...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@BeforeCheck(name="startActReq")
	private void startServer(){
		if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            //Controller controller = new Controller();
            IController stub = (IController) UnicastRemoteObject.exportObject(this, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(CONTROLLER_NAME, stub);
            log.info("Controller bound.");
        } catch (Exception e) {
            System.err.println("Controller exception:");
            e.printStackTrace();
        }
	}
	
	@AfterCheck(name="initJobActReq")
	private void initJob(){
		producedItems++;
		JoinCheckpoint.getInstance().resetCurrentWorker();
		JoinCheckpoint.getInstance().setControllerMode(false);
	}
	
	@AfterCheck(name="newJobActReq")
	private void newJobAction(){
		log.info("New job action request...");
	}
	
	@AfterCheck(name="idleActReq")
	private void idleAction(){
		log.info("Idle action request...");
	}
	
	@AfterCheck(name="resetWorkstation")
	private void resetWorker() {
		log.info("Resetting Worker...");
	}
	
	private void resetWorkers() {
		for(int i=0;i<WORKERS;i++)
			resetWorker();
	}
	

	@Override
	public void run(Object... args) {
		
		JoinCheckpoint.getInstance().setControllerMode(true);
		startServer();
		JoinCheckpoint.getInstance().joinController(WORKERS);
		workersReady();

		while(producedItems < PRODUCTS){
			initJob();

			JoinCheckpoint.getInstance().joinController(WORKERS);
			
			shippingAction();
			resetWorkers();
			newJobAction();
		}
		idleAction();
		
	}
	

	public static void main(String[] args){
		String inputPath = "";
    	
    	// create the command line parser
		CommandLineParser parser = new BasicParser();
		HelpFormatter formatter = new HelpFormatter();

		// create the Options
		Options options = new Options();
		options.addOption( "h", "help", false, "Show the basic usage.");
		options.addOption( "t", "test", false, "Set online testing mode.");
		options.addOption( "m", "model", true, "Set the TB net model path.");


		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );

		    // validate that bucket-name has been set
		    if(line.hasOption("help") || !line.hasOption("model")) {
		    	formatter.printHelp( "Controller [OPTION] ...", options );
				System.exit(0);
		    }
		    
		    if(line.hasOption("model"))
		    	inputPath = line.getOptionValue("model");
		    if(line.hasOption("test"))
		    	Monitor.setOnlineTestingMode(true);
		}
		catch( Exception exp ) {
		    exp.printStackTrace();
		}
		
		Monitor.startMonitorFromFile(new Controller(), inputPath ,(Object[])args);
	}

	@Override
	public void onlineTest(Object... args) {
		run(args);	
	}

}
