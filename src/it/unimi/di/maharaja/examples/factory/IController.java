/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.factory;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IController extends Remote {
	public void joinWorker(Integer id) throws RemoteException;
	public void receivedActionEvent() throws RemoteException;
	public void pickingAction() throws RemoteException;
	public void assemblyAction() throws RemoteException;
	public void placingAction() throws RemoteException;
	public void doneAction() throws RemoteException;
}
