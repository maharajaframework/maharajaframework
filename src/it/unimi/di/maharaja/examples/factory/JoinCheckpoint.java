/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.factory;


public class JoinCheckpoint {
	
	private int waiting = 0;
	private Object[] waitingWorkers = null;
	private Object waitingController = null;
	private int currentWorker = 0;
	private static JoinCheckpoint instance = null;
	private boolean controllerMode = true;
	
	private JoinCheckpoint(){
		waitingWorkers = new Object[Controller.WORKERS];
	}
	
	public static JoinCheckpoint getInstance(){
		if(instance == null)
			instance = new JoinCheckpoint();
		return instance;
	}
	
	public synchronized void resetCurrentWorker(){
		currentWorker = 0;
	}
	
	public synchronized void setControllerMode(boolean mode){
		controllerMode = mode;
	}
	
	public synchronized void joinWorker(int maxWaitingProcesses, int id){
		if(waiting < maxWaitingProcesses){
			waiting++;
			try {
				waitingWorkers[id] = this;
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else {
			if(controllerMode || currentWorker >= Controller.WORKERS){
				waitingController.notify();
				waitingWorkers[id] = this;
				try {
					wait();
				} catch (InterruptedException e) {
					
				}
			}
			else if(currentWorker < Controller.WORKERS){
				if(id != currentWorker){
					waitingWorkers[currentWorker].notify();
					waitingWorkers[id] = this;
					currentWorker++;
					try {
						wait();
					} catch (InterruptedException e) {
						
					}
				}
				else
					currentWorker++;
			}
		}
	}
	
	public synchronized void joinController(int maxWaitingProcesses){
		if(waiting < maxWaitingProcesses){
			waiting++;
			try {
				waitingController = this;
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else {
			if(!controllerMode && currentWorker < Controller.WORKERS){
				waitingWorkers[currentWorker].notify();
				currentWorker++;
				try {
					waitingController = this;
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
