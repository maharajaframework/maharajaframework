/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.factory;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

public class Worker {
	
	private int id;
	private static final Logger log = Logger.getLogger(Worker.class.getName());
	
	public Worker(int id){
		this.id = id;
	}
	
	private void join(String host){
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
	    }
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            IController remoteController = (IController) registry.lookup(Controller.CONTROLLER_NAME);
            //Worker task = new Worker();
            while(true){
            	remoteController.joinWorker(id);
            	log.info("Worker started...");
            	remoteController.receivedActionEvent();
            	remoteController.pickingAction();
            	picking();
            	remoteController.assemblyAction();
            	assembly();
            	remoteController.placingAction();
            	placing();
            	remoteController.doneAction();
            	log.info("Worker done...");
            }
            
        } catch (Exception e) {
            System.err.println("Worker exception:");
            e.printStackTrace();
        }
	}
	
	private void placing() {
		log.info("Placing...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void assembly() {
		log.info("Assembling...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}

	private void picking(){
		log.info("Picking...");
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	public static void main(String[] args){		
		String id = args[0];
		String host = args[1];	
		Worker w = new Worker(new Integer(id));
		w.join(host);
		
	}
}
