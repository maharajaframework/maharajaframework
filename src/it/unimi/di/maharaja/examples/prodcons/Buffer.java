/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.prodcons;

import java.util.concurrent.LinkedBlockingQueue;


public class Buffer {

	private LinkedBlockingQueue<Integer> buffer = null;
	private static Buffer instance = null;
	
	private Buffer(){
		buffer = new LinkedBlockingQueue<Integer>();
	}
	
	public static Buffer getInstance(){
		if(instance == null)
			instance = new Buffer();
		return instance;
	}
	
	public Integer consume(){
		try {
			return buffer.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void produce(Integer data){
		try {
			buffer.put(data);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
