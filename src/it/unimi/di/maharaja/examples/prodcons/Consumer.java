/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.prodcons;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.logging.Logger;

import it.unimi.di.maharaja.monitor.AroundCheck;
import it.unimi.di.maharaja.monitor.AfterCheck;
import it.unimi.di.maharaja.monitor.BeforeCheck;


public class Consumer implements Runnable {
	
	private static final Logger log = Logger.getLogger(Consumer.class.getName());

	@Override
	public void run() {
//		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
//		long start = bean.getThreadCpuTime(Thread.currentThread().getId());
		
		Integer data = consume();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		taskcons(data);
		
//		log.finer("[Consumer] cpu-time " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - start));
	}
	
	@AfterCheck(name="consume")
	private Integer consume(){
		int data = Buffer.getInstance().consume();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("[Consumer] consume data: " + data);
		return data;
	}
	
	@AfterCheck(name="taskcons")
	private void taskcons(Integer data){
		log.info("[Consumer] taskcons.");
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
