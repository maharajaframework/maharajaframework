/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.prodcons;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.logging.Logger;

import it.unimi.di.maharaja.monitor.AfterCheck;
import it.unimi.di.maharaja.monitor.AroundCheck;
import it.unimi.di.maharaja.monitor.BeforeCheck;

public class Producer implements Runnable {
	
	private static final Logger log = Logger.getLogger(Producer.class.getName());
	private static int data = 0;

	@Override
	public void run() {
//		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
//		long start = bean.getThreadCpuTime(Thread.currentThread().getId());		
		taskprod();
		produce();		
//		log.finer("[Producer] cpu-time " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - start));
	}
	
	@BeforeCheck(name="produce")
	private void produce(){
		log.info("[Producer] produce data: " + data);
		Buffer.getInstance().produce(data);
	}
	
	@AroundCheck(before="taskprodStart", after="taskprodEnd")
	private int taskprod(){
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		log.info("[Producer] taskprod.");
		return data++;
	}
	

}
