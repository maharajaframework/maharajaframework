/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.examples.prodcons;

import java.lang.management.ManagementFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

import it.unimi.di.maharaja.monitor.Monitor;
import it.unimi.di.maharaja.monitor.Monitorable;

public class ProducerConsumer extends Monitorable{

    private static final Logger log = Logger.getLogger(ProducerConsumer.class.getName());
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(2);
    

    public static void main(String[] args) { 
    	String inputPath = "";
    	
    	// create the command line parser
		CommandLineParser parser = new BasicParser();
		HelpFormatter formatter = new HelpFormatter();

		// create the Options
		Options options = new Options();
		options.addOption( "h", "help", false, "Show the basic usage.");
		//options.addOption( "t", "test", false, "Online testing mode.");
		options.addOption( "m", "model", true, "Set the TB net model path.");


		try {
		    // parse the command line arguments
		    CommandLine line = parser.parse( options, args );

		    // validate that bucket-name has been set
		    if(line.hasOption("help") || !line.hasOption("model")) {
		    	formatter.printHelp( "ProducerConsumer [OPTION] ...", options );
				System.exit(0);
		    }
		   
//		    if(line.hasOption("test"))
//		    	Monitor.setOnlineTestingMode(true);
		    
		    if(line.hasOption("model"))
		    	inputPath = line.getOptionValue("model");
		}
		catch( Exception exp ) {
		    exp.printStackTrace();
		}
    	
    	Monitor.startMonitorFromFile(new ProducerConsumer(), inputPath ,(Object[])args);
    	//new ProducerConsumer().run((Object[])args);
    }

	@Override
	public void run(Object... args) {		
		
		log.finer("[Thread CPU supported] " + ManagementFactory.getThreadMXBean().isThreadCpuTimeSupported());
		
		scheduler.scheduleAtFixedRate(new Producer(), 1000, 1800, TimeUnit.MILLISECONDS);
		scheduler.scheduleAtFixedRate(new Consumer(), 0, 1500, TimeUnit.MILLISECONDS);
		
		try {
			Thread.sleep(300000); // 60s. monitoring
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		scheduler.shutdown();
		
		try {
			Thread.sleep(4000); // 4s.
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onlineTest(Object... args) {
		run(args);		
	}


}
