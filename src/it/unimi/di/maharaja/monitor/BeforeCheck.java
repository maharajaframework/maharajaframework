package it.unimi.di.maharaja.monitor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface BeforeCheck {
	String name() default Event.NONE;
}
