/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.monitor;

import java.util.logging.Logger;

public class Checkpoint {
	
	private Thread waiting = null;	
	private static Checkpoint instance = null;
	
	private static final Logger log = Logger.getLogger(Checkpoint.class.getName());
	
	public synchronized static Checkpoint getInstance(){
		if(instance == null)
			instance = new Checkpoint();
		return instance;
	}
	
	public synchronized void join(Thread t){
		log.info("[Checkpoint] Thread joined: " + t.getName());
		if(waiting == null){
			waiting = t;
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		else {
			notifyAll();
			waiting = null;
		}
	}

}
