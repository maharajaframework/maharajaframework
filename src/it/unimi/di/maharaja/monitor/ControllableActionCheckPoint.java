/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.monitor;

import java.util.HashMap;
import java.util.List;

enum WeightType {
	FIXED,
	DECREMENTING
};

public class ControllableActionCheckPoint {

	private static ControllableActionCheckPoint instance = null;
	private List<String> actions;
	
	private HashMap<String, Pair<WeightType,Integer>> weights = null;
	
	private ControllableActionCheckPoint(){
		// weights init
		weights = new HashMap<String, Pair<WeightType, Integer>>();
		weights.put("moveActReq", new Pair(WeightType.DECREMENTING, 10));
		weights.put("openActReq", new Pair(WeightType.FIXED, 10));
		weights.put("interrupt", new Pair(WeightType.FIXED, 10));
		weights.put("timeout", new Pair(WeightType.FIXED, 10));
		weights.put("interrupt2", new Pair(WeightType.FIXED, 10));
		weights.put("none", new Pair(WeightType.FIXED, 10));
		
	}
	
	public synchronized static ControllableActionCheckPoint getInstance(){
		if(instance == null)
			instance = new ControllableActionCheckPoint();
		return instance;
	}
	
	public synchronized void pushControllableActions(List<String> actions){
		this.actions = actions;
		notifyAll();
	}
	
	public synchronized List<String> waitForControllableActions(){
		if(actions == null){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		List<String> ret = actions;
		actions = null;
		
		return ret;
	}
	
	public String randomChoiceWithNone(List<String> actions){
		actions.add("none");
		return randomChoice(actions);
	}
	
	public String randomChoice(List<String> actions){
		HashMap<String, Pair<WeightType, Integer>> selectableActions = new HashMap<String, Pair<WeightType, Integer>>(); 
		int totalWeight = 0;
		for(String a: actions){
			if(weights.containsKey(a)){
				Pair<WeightType, Integer> pair = weights.get(a);
				if(pair.getSecond() > 0){
					selectableActions.put(a, pair);
					totalWeight += pair.getSecond();
				}
			}
		}
		double r = Math.random() * totalWeight;
		double countWeight = 0.0;
		for(String a: selectableActions.keySet()){
			Pair<WeightType, Integer> pair = weights.get(a);
			countWeight += pair.getSecond();
			if(countWeight >= r){
				if(pair.getFirst() == WeightType.DECREMENTING)
					weights.put(a, new Pair(WeightType.DECREMENTING, pair.getSecond()-1));
				return a;
			}
		}
		
		return null;
	}
	
	
	class Pair<A, B> {
	    private A first;
	    private B second;

	    public Pair(A first, B second) {
	    	super();
	    	this.first = first;
	    	this.second = second;
	    }

	    public int hashCode() {
	    	int hashFirst = first != null ? first.hashCode() : 0;
	    	int hashSecond = second != null ? second.hashCode() : 0;

	    	return (hashFirst + hashSecond) * hashSecond + hashFirst;
	    }

	    public String toString()
	    { 
	           return "(" + first + ", " + second + ")"; 
	    }

	    public A getFirst() {
	    	return first;
	    }

	    public void setFirst(A first) {
	    	this.first = first;
	    }

	    public B getSecond() {
	    	return second;
	    }

	    public void setSecond(B second) {
	    	this.second = second;
	    }
	}
}
