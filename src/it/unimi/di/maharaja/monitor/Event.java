/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */


package it.unimi.di.maharaja.monitor;

public class Event {
	
	private String name;
	private long start;
	private long end;
	
	public static final String NONE = "NONE";
	private static final String STOP = "STOP";
	private static final long DEFAULT = -1L;
	
	public static Event beforeEvent(String name, long start){
		return new Event(name, start, DEFAULT);
	}
	
	public static Event afterEvent(String name, long end){
		return new Event(name, DEFAULT, end);
	}
	public static Event aroundEvent(String name,long start,  long end){
		return new Event(name, start, end);
	}
	
	
	private Event(String name, long start, long end){
		this.name = name;
		this.start = start;
		this.end = end;
	}
	
	
	public Event(String name){
		this.name = name;
		this.start = DEFAULT;
		this.end = DEFAULT;
	}
	
	public static Event stopEvent(){
		return new Event(STOP);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}
	
	public long getTime(){
		if(end != DEFAULT)
			return end;
		else
			return start;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public String toString(){
		return name + " (" + start + ", " + end + ")";
	}
	
	public boolean isStop(){
		return name.equals(STOP);
	}
	
	public boolean timeCheck(long lastEventTime, double tmin, double tmax){
		if(start != DEFAULT && end != DEFAULT)
			return (start-lastEventTime) >= tmin && (end-lastEventTime) <= tmax; // around check
		else if(start==DEFAULT  && end != DEFAULT)
			return (end-lastEventTime) >= tmin && (end-lastEventTime) <= tmax; // after check
		else if(start != DEFAULT && end == DEFAULT)
			return (start-lastEventTime) >= tmin && (start -lastEventTime) <= tmax; // before check
		else return false; // invalid event object
	}

}
