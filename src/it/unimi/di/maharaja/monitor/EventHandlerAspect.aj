/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */

package it.unimi.di.maharaja.monitor;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.logging.Logger;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class EventHandlerAspect {

    private static final Logger log = Logger.getLogger(EventHandlerAspect.class.getName());
    private ThreadMXBean bean = ManagementFactory.getThreadMXBean();
    
    @Pointcut("execution(@AroundCheck * *(..))")
    void aroundTransitionMethod() {}
    
    @Pointcut("execution(@AfterCheck * *(..))")
    void afterTransitionMethod() {}
    
    @Pointcut("execution(@BeforeCheck * *(..))")
    void beforeTransitionMethod() {}
    

    @Around(value="aroundTransitionMethod() && @annotation(trans)", argNames = "trans")
    public Object aroundCheckFiringTransition(ProceedingJoinPoint thisJoinPoint, AroundCheck trans) {
    	
    	long MIOStart = bean.getThreadCpuTime(Thread.currentThread().getId());
    	long start = System.currentTimeMillis();
    	try {
			Monitor m = Monitor.getInstance();
			m.check(Event.beforeEvent(trans.before(), start));
		} catch (Exception e) {
			e.printStackTrace();
		}
    	log.finer("MIO around(1) = " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - MIOStart) + " ns.");
    	
    	Object ret = null;
        try {
			ret = thisJoinPoint.proceed(thisJoinPoint.getArgs());
		} catch (Throwable e) {
			e.printStackTrace();
		}
        long end = System.currentTimeMillis();        
        MIOStart = bean.getThreadCpuTime(Thread.currentThread().getId());
        
//        if(eventName.equals(Event.NONE))
//    		eventName = extractEventName(thisJoinPoint);
        
        try {
			Monitor m = Monitor.getInstance();
			m.check(Event.afterEvent(trans.after(), end));
		} catch (Exception e) {
			e.printStackTrace();
		}
        log.finer("MIO around(2) = " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - MIOStart) + " ns.");

        return ret;
    }
    
    @After(value="afterTransitionMethod() && @annotation(trans)", argNames="trans")
    public void afterCheckFiringTransition(JoinPoint thisJoinPoint, AfterCheck trans) {
    	long end = System.currentTimeMillis();
    	//log.info("in @After transition = " + trans.name() + " TIME =" + end);
    	long MIOStart = bean.getThreadCpuTime(Thread.currentThread().getId());
    	String eventName = trans.name();
//    	if(eventName.equals(Event.NONE))
//    		eventName = extractEventName(thisJoinPoint);
        try {
			Monitor m = Monitor.getInstance();
			m.check(Event.afterEvent(eventName, end));
		} catch (Exception e) {
			e.printStackTrace();
		}
        log.finer("MIO after = " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - MIOStart) + " ns.");
    }
    
    @Before(value="beforeTransitionMethod() && @annotation(trans)", argNames="trans")
    public void beforeCheckFiringTransition(JoinPoint thisJoinPoint, BeforeCheck trans) {
    	long start = System.currentTimeMillis();
    	//log.info("in @After transition = " + trans.name() + " TIME =" + end);
    	long MIOStart = bean.getThreadCpuTime(Thread.currentThread().getId());
    	String eventName = trans.name();
//    	if(eventName.equals(Event.NONE))
//    		eventName = extractEventName(thisJoinPoint);
        try {
			Monitor m = Monitor.getInstance();
			m.check(Event.beforeEvent(eventName, start));
		} catch (Exception e) {
			e.printStackTrace();
		}
        log.finer("MIO before = " + (bean.getThreadCpuTime(Thread.currentThread().getId()) - MIOStart) + " ns.");
    }
    
//    private String extractEventName(JoinPoint thisJoinPoint){
//    	String eventName = null;
//    	Field[] fields = thisJoinPoint.getThis().getClass().getDeclaredFields();
//		for(Field f: fields){
//			if(Modifier.isPublic(f.getModifiers())){
//				TransitionName annotation = f.getAnnotation(TransitionName.class);
//				if(annotation != null && annotation.method().equals(thisJoinPoint.getSignature().getName())){
//					try {
//						eventName = (String)f.get(thisJoinPoint.getThis());
//					} catch (IllegalArgumentException e) {
//						e.printStackTrace();
//					} catch (IllegalAccessException e) {
//						e.printStackTrace();
//					}
//					break;
//				}
//			}
//		}
//		return eventName;
//    }

}
