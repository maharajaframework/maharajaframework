/**
 * This file is part of MahaRAJA.
 * 
 *  MahaRAJA is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  MahaRAJA is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MahaRAJA. If not, see <http://www.gnu.org/licenses/>.
 *       
 * @author Matteo Camilli <matteo.camilli@unimi.it>
 *
 */


package it.unimi.di.maharaja.monitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import dataLayer.TBNet;
import algebra.Constraint;
import analysis.EnabContainer;
import analysis.Graph;
import analysis.GraphNode;
import analysis.SymbolicState;
import main.Graphgen;
import net.openhft.affinity.AffinityLock;
import net.openhft.affinity.AffinityStrategies;
import net.openhft.affinity.AffinitySupport;


public class Monitor {
	
	private static final Logger log = Logger.getLogger(Monitor.class.getName());
	
	private static Monitor instance = null;	
	private LinkedBlockingQueue<Event> queue = null;
	private Long lastEventTime = 0L;
	private Long startEventTime = 0L;
	//private GraphNode currentNode = null;
	private List<GraphNode> currentNodes = null;
	private Monitorable subject = null;
	
	private static boolean onlineTestingMode = false;
	private Map<String, Constraint> controllableStates = null;
	private Map<String, Constraint> goals = null;
	private List<GraphNode> trace = null;
	
	private Monitor(){
		queue = new LinkedBlockingQueue<Event>();
		currentNodes = new ArrayList<GraphNode>();
		if(onlineTestingMode){
			controllableStates = new HashMap<String, Constraint>();
			goals = new HashMap<String, Constraint>();
			trace = new ArrayList<GraphNode>();
		}
	}
	
	public static Monitor getInstance() throws Exception{
		if(instance != null)
			return instance;
		else
			throw new Exception("Monitor not started.");
	}
	
	public static void startMonitorFromFile(Monitorable subject, String inputPath, Object... args){
		if(instance == null){
			File f = new File(inputPath);
			if(!f.exists())
				try {
					throw new FileNotFoundException();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
			}
			
			TBNet net = new TBNet("model");
			net.buildFromFile(inputPath);
			startMonitor(subject, net, args);
		}
	}
	
	public static void startMonitorFromInputStream(Monitorable subject, InputStream inputStream, Object... args){
		if(instance == null){
			TBNet net = new TBNet("model");
			net.buildFromInputStream(inputStream);
			startMonitor(subject, net, args);
		}
	}
	
	private static void startMonitor(Monitorable subject, final TBNet net, Object... args){
		if(instance == null){
			
			final AffinityLock al = AffinityLock.acquireLock();
			try {
				log.info("[" + AffinitySupport.getThreadId() + "] Main locked: CPU " + al.cpuId());
		        
			    Thread t = new Thread(new Runnable() {
			        @Override
			        public void run() {
			        	try (AffinityLock al2 = al.acquireLock(AffinityStrategies.DIFFERENT_SOCKET, AffinityStrategies.DIFFERENT_CORE, AffinityStrategies.ANY)) {
			        		log.info("[" + AffinitySupport.getThreadId() + "] Monitor locked: CPU " + al2.cpuId());
							 
							 instance = new Monitor();
							 if(onlineTestingMode){
								 instance.configGoals();
								 instance.configControllableStates();
							 }
								 
							 //instance.currentNode = instance.buildRootState(net);
							 instance.currentNodes.add(instance.buildRootState(net));
							 
							 if(onlineTestingMode)
								 for(GraphNode cn: instance.currentNodes)
								 instance.checkTestStep(cn, net);
							
							 Checkpoint.getInstance().join(Thread.currentThread());
							 instance.start(net);
			        
			            }
			        }
			    });
			    t.start();
			    
			    Checkpoint.getInstance().join(Thread.currentThread());
			    
			    //log.info("\nThe assignment of CPUs is\n" + AffinityLock.dumpLocks());
			    instance.subject = subject;
			    instance.startEventTime = System.currentTimeMillis();
			    instance.lastEventTime = System.currentTimeMillis();
			    log.info("STARTING THE SUBJECT " + instance.lastEventTime);
			    
			    if(onlineTestingMode)
			    	subject.onlineTest(al, args);
			    else
			    	subject.run(al, args);
			    
			    instance.stopMonitor();
			    
			    Checkpoint.getInstance().join(Thread.currentThread());
			    log.info("SUBJECT STOPPED...");	
			    
			} finally {
			     al.release();
			}
		}
		
	}
	
	private void configReachableStates(Map<String, Constraint> map, String label){
		try {
			BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
			System.out.println(label);
			System.out.println("Marking (leave empty to finish): ");
			String line = "";
			String marking = "";
			Constraint constraint = null;
			while (!(line = stdin.readLine()).equals("")){
				marking = line;
				System.out.println("Contraint (leave empty for TRUE): ");
				line = stdin.readLine();
				if(line.equals(""))
					constraint = new Constraint();
				else
					constraint = new Constraint(line);
				
				map.put(marking, constraint);
				
				System.out.println(label);
				System.out.println("Marking (leave empty to finish): ");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void configControllableStates() {
		configReachableStates(controllableStates, "Insert a controllable state:");
	}

	private void configGoals() {
		// e.g., goals.put("Buffer{T1;}C1{T0;}P1{T1;}", new Constraint("TL>=10000"));
		configReachableStates(goals, "Insert a goal:");
	}

	private GraphNode buildRootState(TBNet net) {
		Graphgen.RelativeTime = false;
		Graphgen.TimeAnonymous=false;
		
		Graph trg = new Graph(net);
		GraphNode currentNode = trg.getRoot();
		currentNode.setName("Sc");
		String lt=currentNode.getState().findLastTime();
		currentNode.putNewVar(lt);
		String temp=currentNode.getState().getPrintableConstraint();
		if(!temp.contains("TL"))
			temp= "("+ temp + ") && TL==" + lt;
		currentNode.getState().putConstraints(temp);

		currentNode.printReduced();
		currentNode.normalizeNames();
		
		currentNode.putNormConstraint(currentNode.expandedConstraint().toString());
		
		log.info("Root created." /*+ currentNode*/);
		
		return currentNode;
	}

	private void start(TBNet net) {
		log.info("MONITOR STARTED...");
		while(true){
			try {
				Event event = queue.take();
				if(event.isStop()){
					Checkpoint.getInstance().join(Thread.currentThread());
					log.info("MONITOR STOPPED...");
					break;
				}
				if(!checkEvent(event, net))
					try {
						subject.addError(event);
						throw new Exception("Invalid event: " + event);
					} catch (Exception e) {
						e.printStackTrace();
						log.info("Current states: ");
						for(GraphNode cn: currentNodes)
							log.info(cn.toString());
						
						if(onlineTestingMode){
							log.info("**** TEST FAILED ****");
							log.info("TRACE:\n");
							for(GraphNode n: trace)
								log.info("(" + n.getNormMark() + ", " + n.getNormConstraint() + ")");							
							System.exit(1);
						}
					}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
	}
	
	
	private boolean checkEvent(Event event, TBNet net) {
		log.info("[Monitor] " + event);
		//log.finer("OUT: " + System.nanoTime());
		
		String[] transitions = null;
		if(event.getName().contains(","))
			transitions = event.getName().split(",");
		
		List<GraphNode> nextStates = new ArrayList<GraphNode>();
		
		boolean nextComputed = false;
		for(GraphNode cn: currentNodes){
			ArrayList<EnabContainer> activeEnablings = cn.getState().getEnablings(net);
			String newvar=cn.getState().generateNewVariable();
			
			for(EnabContainer enab: activeEnablings){
				
				boolean isTransitionValid=false;
				if(transitions==null)
					isTransitionValid = enab.getEnabling().getTransition().getName().equals(event.getName());
				else{
					/* caso in cui il metodo e' annotato con piu' transizioni es: "tr1,tr2,..." */
					for(int i=0;i<transitions.length;i++){
						if(transitions[i].equals(enab.getEnabling().getTransition().getName())){
							isTransitionValid = true;
							event.setName(transitions[i]);
						}
					}
				}
				
				if(isTransitionValid && timeCheck(enab, event)){
					long currentTime = event.getTime();
					SymbolicState newState=cn.getState().nexState(enab, net, null, null);
					Constraint nc = newState.getConstraints();
					newState.putConstraints("(" + nc.toString() + ") && TL==" + (currentTime-startEventTime));
					
					GraphNode newNode=new GraphNode("Sc", newState, newvar);
					
					newNode.printReduced();
					newNode.normalizeNames();
					
					newNode.identifyEqualsVar(false);

					Constraint cstnew = newNode.expandedConstraint();
					newNode.putNormConstraint(cstnew.toString());
					
					//lastEventTime = event.getTime();
					nextStates.add(newNode);
					
					if(onlineTestingMode)
						checkTestStep(newNode, net);
					
					nextComputed = true;
				}			
			}
		}
		
		//System.err.println("Expected events: " + activeEnablings);
		if(nextComputed){
			lastEventTime = event.getTime();
			currentNodes = nextStates;
			return true;
		}
		else {
			lastEventTime = event.getEnd();
			return false;
		}
		
	}
	
	private void checkTestStep(GraphNode node, TBNet net){
		trace.add(node);
		
		if(isGoalState(node)){
			log.info("**** TEST SUCCESSFUL ****");
			log.info("GOAL REACHED\n" + node.toString());
			System.exit(0);
		}
		else if(isControllableState(node)){
			ArrayList<EnabContainer> ens = node.getState().getEnablings(net);
			List<String> actions = new ArrayList<String>();
			for(EnabContainer ec: ens)
				actions.add(ec.getEnabling().getTransition().getName());
			
			ControllableActionCheckPoint.getInstance().pushControllableActions(actions);
		}
	}
	
	private boolean isGoalState(GraphNode node) {
		Constraint c = goals.get(node.getNormMark());
		if(c != null) {
			if(node.getState().getConstraints().implies(c))
				return true;
			else
				return node.getState().getConstraints().impliesFull(c);
		}
		return false;
	}

	private boolean isControllableState(GraphNode node) {
		Constraint c = controllableStates.get(node.getNormMark());
		if(c != null) {
			if(node.getState().getConstraints().implies(c))
				return true;
			else
				return node.getState().getConstraints().impliesFull(c);
		}
		return false;
	}

	private boolean timeCheck(EnabContainer enab, Event event){
		extractMinMaxFromEdge(enab);
		return event.timeCheck(lastEventTime, enab.getMin(), enab.getMax());
	}

	private void stopMonitor(){
		instance.queue.add(Event.stopEvent());
		instance = null;
	}

	public void check(Event event){
		try {
			//log.finer("IN: " + System.nanoTime());
			queue.put(event);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	private void extractMinMaxFromEdge(EnabContainer ec) {
		Constraint newcst=ec.getCondition();
		try {
			
			ArrayList<String> reducedVars = new ArrayList<String>();
			reducedVars.add("TL");
			reducedVars.add("TLO");
			newcst = newcst.clumber(reducedVars, true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		ec.setMin(newcst.extractMin());
		ec.setMax(newcst.extractMax());
	}
	
	public static void setOnlineTestingMode(boolean mode){
		onlineTestingMode = mode;
	}
	
	public void addControllableState(String m, Constraint c){
		controllableStates.put(m, c);
	}
	
	public void addGoal(String m, Constraint c){
		goals.put(m, c);
	}

}
